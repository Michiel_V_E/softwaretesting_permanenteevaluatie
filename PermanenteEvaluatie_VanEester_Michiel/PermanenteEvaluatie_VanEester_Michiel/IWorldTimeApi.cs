﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PermanenteEvaluatie_VanEester_Michiel
{
    public interface IWorldTimeApi
    {
        int GetCurrentTime();

        string GetCurrentPlaceWithIp();
    }
}
