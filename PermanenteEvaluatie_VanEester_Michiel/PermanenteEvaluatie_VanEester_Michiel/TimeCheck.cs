﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PermanenteEvaluatie_VanEester_Michiel
{
    public class TimeCheck
    {
        private readonly IWorldTimeApi worldTimeApi;

        public TimeCheck(IWorldTimeApi worldTimeApi)
        {
            this.worldTimeApi = worldTimeApi;
        }

        public string GetCurrentTime()
        {
            int time = worldTimeApi.GetCurrentTime();
            if (time >= 0 && time < 7)
            {
                return "nacht";
            }
            if (time >= 7 && time < 12)
            {
                return "voormiddag";
            }
            if (time >= 12 && time < 13)
            {
                return "middag";
            }
            if (time >= 13 && time < 17)
            {
                return "namiddag";
            }
            if (time >= 17)
            {
                return "avond";
            }
            return "Error, time not reconized";
        }

        public string GetLocation()
        {
            var loc = worldTimeApi.GetCurrentPlaceWithIp();
            loc = loc.Replace('_', ' ');
            string[] locationArray = loc.Split('/');
            return locationArray[1];
        }
    }
}
