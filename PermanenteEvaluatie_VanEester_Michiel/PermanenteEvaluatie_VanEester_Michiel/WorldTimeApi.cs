﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json.Serialization;

namespace PermanenteEvaluatie_VanEester_Michiel
{
    public class WorldTimeApi : IWorldTimeApi
    {
        
        private string _Ip { get; set; }

        public WorldTimeApi(string ip)
        {
            _Ip = ip;
        }
        
        public int GetCurrentTime()
        {
            string url = $"http://worldtimeapi.org/api/ip/{_Ip}";

            using var httpClient = new HttpClient();
            var httpResponse = httpClient.GetAsync(url).GetAwaiter().GetResult();
            var response = httpResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            return JsonConvert.DeserializeObject<WorldTime>(response).Datetime.Hour;
            
        }

        public string GetCurrentPlaceWithIp()
        {
            string url = $"http://worldtimeapi.org/api/ip/{_Ip}";

            using var httpClient = new HttpClient();
            var httpResponse = httpClient.GetAsync(url).GetAwaiter().GetResult();
            var response = httpResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            return JsonConvert.DeserializeObject<WorldTime>(response).Timezone;  
        }
    }
}
