﻿using System;

namespace PermanenteEvaluatie_VanEester_Michiel
{
    class Program
    {
        static void Main(string[] args)
        {
            

            Console.WriteLine("Geef een IP adres op (xxx.xxx.xxx.xxx):");
            string ip = Console.ReadLine();

            var worldTimeApi = new WorldTimeApi(ip);
            var openWeatherApi = new OpenWeatherApi();
            var timeCheck = new TimeCheck(worldTimeApi);

            string outputTime = timeCheck.GetCurrentTime();
            string outputLocation = timeCheck.GetLocation();
            float outputWeather = openWeatherApi.GetCurrentTemperature(outputLocation);

            Console.WriteLine($"Op dit IP is het momenteel {outputTime}");
            Console.WriteLine($"Het is op deze locatie {outputWeather} graden");
        }
    }
}
