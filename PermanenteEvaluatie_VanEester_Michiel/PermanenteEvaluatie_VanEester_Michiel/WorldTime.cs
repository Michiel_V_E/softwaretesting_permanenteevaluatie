﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PermanenteEvaluatie_VanEester_Michiel
{
    class WorldTime
    {
        public DateTimeOffset Datetime { get; set; }
        public string utc_offset { get; set; }
        public string Timezone { get; set; }
    }
}
